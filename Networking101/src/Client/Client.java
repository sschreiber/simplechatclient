package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client 
{
	private Socket connection;
	private int port;
	private String serverIPAddress;
	private BufferedReader networkInputStream;
	private BufferedReader stdInputStream;
	private PrintWriter networkOutputStream;
	
	public Client(String serverIPAddress, int port) throws UnknownHostException, IOException 
	{
		connection = new Socket(serverIPAddress, port);
		networkOutputStream = new PrintWriter(connection.getOutputStream(), true);
		networkInputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		stdInputStream = new BufferedReader(new InputStreamReader(System.in));
		startReadingMsg();
	}
	
	public void sendMessage()
	{
		System.out.println("Type in the message you want to send:");
		String message;
		try {
			message = stdInputStream.readLine();
			networkOutputStream.println(message);
			
			networkOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void startReadingMsg()
	{
		Thread readingThread = new Thread(new ReadMessages(networkInputStream));
		readingThread.start();
	}
	
	public static void main(String[] args) 
	{
		int port = 3001;
		String serverIPAddress = "127.0.0.1";
		try {
			Client currentClient = new Client(serverIPAddress, port);
			while (true) {
				currentClient.sendMessage();
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
