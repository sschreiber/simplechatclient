package Server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientData {
	private Socket clientConnection;
	private BufferedReader networkInputStream;
	private PrintWriter networkOutputStream;
	private long id;
	
	public ClientData(long id, Socket clientConnection) {
		this.id = id;
		this.clientConnection = clientConnection;
		try {
			networkInputStream = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));
			networkOutputStream = new PrintWriter(clientConnection.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public BufferedReader getNetworkInputStream() {
		return networkInputStream;
	}
	
	public PrintWriter getNetworkOutputStream() {
		return networkOutputStream;
	}

	public Long getID() {
		// TODO Auto-generated method stub
		return id;
	}
}
