package Server;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class MsgServer {
	
	private ConcurrentHashMap<Long, ClientData> clientConnections;
	private ServerSocket serverSocket;
	private int port;
	
	public MsgServer(int port) throws IOException
	{
		serverSocket = new ServerSocket(port);
		clientConnections = new ConcurrentHashMap<>();
		this.port = port;
	}
	
	public void startServer()
	{
		initServer();
		handleMessages();
	}
	
	private void initServer() 
	{
		Thread connectionThread = new Thread(new AcceptingConnections(serverSocket, clientConnections));
		connectionThread.start();
	}
	
	private void handleMessages()
	{
		while (true) {
			Enumeration<Long> clientIDs = clientConnections.keys();
		
			 while (clientIDs.hasMoreElements()) {
				Long currentID = clientIDs.nextElement();
				ClientData currentClient = clientConnections.get(currentID);
				String message = null;
				try {
					if (currentClient.getNetworkInputStream().ready()) {
						message = currentClient.getNetworkInputStream().readLine();
						System.out.println(message);
						
					}
					
				
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (message != null) {
					broadCastMessage(currentID, message);
				}
			}
		}
	}
	
	private void broadCastMessage(Long senderID, String message) 
	{
		System.out.println("BroadCasting message");
		Enumeration<Long> clientIDs = clientConnections.keys();
		 while (clientIDs.hasMoreElements()) {
			 Long currentID = clientIDs.nextElement();
			 if (!currentID.equals(senderID)) {
				 System.out.println("Sending to " + currentID);
				 ClientData currentClient = clientConnections.get(currentID);
				 currentClient.getNetworkOutputStream().println(message);
			 }
		 }
	}
	
	public static void main(String [] args) 
	{
		int port = 3001;
		try {

			MsgServer msgSever = new MsgServer(port);
			msgSever.startServer();
		} catch (IOException e) {
			System.err.println("Unable to start server");
		}
	}
	
}
