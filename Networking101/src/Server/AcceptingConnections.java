package Server;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ClientInfoStatus;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;

public class AcceptingConnections implements Runnable {
	private ConcurrentHashMap<Long, ClientData> clientConnections;
	private ServerSocket sSocket;
	long uniqueID = 0;
	public AcceptingConnections(ServerSocket sSocket, ConcurrentHashMap<Long, ClientData> clientConnections) {
		this.sSocket = sSocket;
		this.clientConnections = clientConnections;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Socket clientConnection = sSocket.accept();
				ClientData newClient = new ClientData(uniqueID, clientConnection);
				uniqueID++;
				System.out.println(clientConnection.getLocalAddress());
				clientConnections.put(newClient.getID(), newClient);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
		}
	}

}
